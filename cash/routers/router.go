package routers

import (
	"cash/cash/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/user", &controllers.UserController{})
	beego.Router("/monitor/InsertInfo",&controllers.MonitorController{}, "*:InsertInfo")
	beego.Router("/monitor/QueryData",&controllers.MonitorController{}, "*:QueryData")

}
