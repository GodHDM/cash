package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"

	_ "github.com/go-sql-driver/mysql"

	"fmt"
	"time"
)

//var(
//	id    int
//	name  int
//	age   string
//	email string
//)

type image struct {

}

type user struct {
	Id      int		`form:"_"`
	Name    int 	`form:"username" `
	Age     int     `form:"age"`
	Email   string  `form:"email"`
	Phone   string  `form:"phone"`
}

// define the controller
type UserController struct {
	beego.Controller
}

func (c *UserController) Get() {
	//o :=  orm.NewOrm()
	//o.Using("default")
	////
	//o.Raw("select Id, Name, Age, Email from user").QueryRow(&id, &name, &age, &email)
	//c.Data["id"]    = id
	//c.Data["name"]  = name
	//c.Data["age"]   = age
	//c.Data["email"] = email

	c.TplName = "register.html"
}

func (c *UserController) Post(){
	f ,h, err := c.GetFile("uploadname")
	fmt.Println(h.Filename)
	defer f.Close()
	date := time.Now().Format("2006-01-02 15:04:05")

	if err != nil {
		fmt.Println("getfile error")
	}else{
		c.SaveToFile("uploadname", "static/upload/" + date+ h.Filename)
	}

	c.Data["image"] = "static/upload/"+ date+ h.Filename
	c.TplName = "register.html"
}

func (c *UserController) Register() {
	o := orm.NewOrm()
	o.Using("default")

	//parse form for view,insert data into mysql
	user := user{}
	if err := c.ParseForm(&user); err != nil {
		beego.Info(err)
		return
	}else {
		fmt.Println(user.Name, user.Age, user.Email, user.Phone)
		o.Insert(&user)
	}
}

//func (c *UserController) Show(){
//
//}


func init()  {
	orm.RegisterModel(new(user))
	orm.RegisterModel(new(memory))

	orm.RegisterDriver("mysql", orm.DRMySQL)
	orm.RegisterDataBase("default", "mysql", "root:st@tcp(127.0.0.1:3306)/cash?charset=utf8")
	orm.RunSyncdb("default", false, true)
}