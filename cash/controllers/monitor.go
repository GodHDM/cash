package controllers

import (
	"fmt"
	"time"

	"github.com/astaxie/beego"
	"github.com/shirou/gopsutil/mem"
	"github.com/astaxie/beego/orm"
)


type memory struct {
	Id		   int		  `json:"_"`
	Percent	   float64 	  `json:"percent"`
	SwapCache  uint64     `json:"swap_cache"`
	Expire	   string	  `json:"expire"`
}

type MonitorController struct {
	beego.Controller
}

func (m *MonitorController) InsertInfo() {
	//the demo for five second ticker
	for  {
		Delay := time.NewTicker(5*time.Second)
		select {
		case<- Delay.C:
			go func() {
				o := orm.NewOrm()
				v, _ := mem.VirtualMemory()
				user := memory{
					Percent: 	v.UsedPercent,
					SwapCache:  v.SwapCached,
					Expire: 	time.Now().Format("2006-01-02 15:04:05"),
				}
				fmt.Println(v.UsedPercent, v.SwapCached, time.Now().Format("2006-01-02 15:04:05"))
				o.Insert(&user)
			}()
		}
	}
}



func (t * MonitorController) QueryData() {
	// use sql operation

}
