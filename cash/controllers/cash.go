package controllers

import (
	"github.com/astaxie/beego"
	"fmt"
	"time"
)

// define the controller
type CashController struct {
	beego.Controller
}


func (c *CashController) Get(){
	f ,h, err := c.GetFile("uploadname")
	fmt.Println(h.Filename)
	defer f.Close()
	date := time.Now().Format("2006-01-02 15:04:05")

	if err != nil {
		fmt.Println("getfile error")
	}else{
		c.SaveToFile("uploadname", "static/upload/" + date+ h.Filename)
	}

	c.Data["image"] = "static/upload/"+ date+ h.Filename
	c.TplName = "register.html"
}
